package oak.forge.domain.gamesessionclient


import groovy.util.logging.Slf4j
import oak.forge.domain.GameSessionService
import oak.forge.commons.bus.guava.GuavaMessageBus
import oak.forge.commons.data.message.Message
import oak.forge.domain.event.ClientGenericError
import oak.forge.domain.event.CreateGameSessionFailedError
import oak.forge.domain.event.GameSessionCreatedEvent
import oak.forge.domain.local.GameSessionAggregateFake
import oak.forge.domain.model.CreateGameSessionDto
import com.google.common.eventbus.EventBus

import spock.lang.Specification
import spock.lang.Subject

import static java.util.UUID.randomUUID

@Slf4j
class CreateGameSessionTest extends Specification {
    private GuavaMessageBus messageBus = new GuavaMessageBus(new EventBus())
    private List<Message> capturedMessages = new ArrayList<>()

    def setup() {
        new GameSessionAggregateFake(messageBus)

        messageBus.registerMessageHandler(GameSessionCreatedEvent.class, e -> {
            log.info("created")
            capturedMessages.push(e)
        })

        messageBus.registerMessageHandler(CreateGameSessionFailedError.class, e -> {
            log.error("not created")
            capturedMessages.push(e)
        })

        messageBus.registerMessageHandler(ClientGenericError.class, e -> {
            log.error("generic error")
            capturedMessages.push(e)
        })
    }

    @Subject
    GameSessionService gameSessionService = new GameSessionService(messageBus)

    def "should create game session"() {
        given:
        CreateGameSessionDto createGameSessionDto = new CreateGameSessionDto(5, "owner", GameSessionAggregateFake.CORRECT_GAME_SESSION_NAME, randomUUID())

        when:
        gameSessionService.createGameSession(createGameSessionDto)

        then:
        GameSessionCreatedEvent event = capturedMessages.head() as GameSessionCreatedEvent
        event.gameSessionName == GameSessionAggregateFake.CORRECT_GAME_SESSION_NAME
    }

    def "should throw error when creating game session"() {
        given:
        CreateGameSessionDto createGameSessionDto = new CreateGameSessionDto(5, "owner", GameSessionAggregateFake.INCORRECT_GAME_SESSION_NAME, randomUUID())

        when:
        gameSessionService.createGameSession(createGameSessionDto)

        then:
        CreateGameSessionFailedError error = capturedMessages.head() as CreateGameSessionFailedError
        error.gameSessionName == GameSessionAggregateFake.INCORRECT_GAME_SESSION_NAME
    }

    def "should throw generic error when creating game session"() {
        given:
        CreateGameSessionDto createGameSessionDto = new CreateGameSessionDto(5, "owner", GameSessionAggregateFake.GENERIC_ERROR, randomUUID())
        when:
        gameSessionService.createGameSession(createGameSessionDto)
        then:
        ClientGenericError error = capturedMessages.head() as ClientGenericError
        error.message == GameSessionAggregateFake.GENERIC_ERROR
    }

}
