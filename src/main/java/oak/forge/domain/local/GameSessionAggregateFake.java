package oak.forge.domain.local;


import lombok.extern.slf4j.*;
import oak.forge.commons.bus.*;
import oak.forge.domain.command.CreateGameSessionCommand;
import oak.forge.domain.event.ClientGenericError;
import oak.forge.domain.event.CreateGameSessionFailedError;
import oak.forge.domain.event.GameSessionCreatedEvent;


import java.util.*;

import static java.util.List.*;

@Slf4j
public class GameSessionAggregateFake extends CommandDriven {

    public static final String CORRECT_GAME_SESSION_NAME = "game session";
    public static final String INCORRECT_GAME_SESSION_NAME = "session error";
    public static final String GENERIC_ERROR = "generic error";

    public GameSessionAggregateFake(MessageBus messageBus) {
        super(messageBus);
        registerCommandHandler(CreateGameSessionCommand.class, this::handleSessionCreation);
    }

    private void handleSessionCreation(CreateGameSessionCommand command) {
        log.info("creating game session");

        if (INCORRECT_GAME_SESSION_NAME.equals(command.getGameSessionName())) {
            CreateGameSessionFailedError error =
                    new CreateGameSessionFailedError("game session creation failure", command.getGameSessionName(), command.getCreatorId().toString());
            post(error);
        }  else if (GENERIC_ERROR.equals(command.getGameSessionName())) {
            ClientGenericError error =
                    new ClientGenericError("generic error");
            post(error);
        } else {
            GameSessionCreatedEvent event = new GameSessionCreatedEvent(
                    command.getMaxNumberOfPlayers(),
                    of(command.getCreatorId()),
                    command.getGameSessionName(),
                    UUID.randomUUID(),
                    command.getCreatorId()
            );
            post(event);
        }
    }

}
