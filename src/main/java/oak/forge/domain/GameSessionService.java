package oak.forge.domain;

import lombok.*;
import oak.forge.commons.bus.*;
import oak.forge.domain.command.CreateGameSessionCommand;
import oak.forge.domain.model.CreateGameSessionDto;

@RequiredArgsConstructor
public class GameSessionService {

    private final MessageBus messageBus;

    public void createGameSession(CreateGameSessionDto dto) {
        CreateGameSessionCommand command = new CreateGameSessionCommand(
                dto.getPlayerId(),
                dto.getGameSessionName(),
                dto.getMaxNumberOfPlayers(),
                dto.getGameOwnerName());
        messageBus.post(command);
    }

}
