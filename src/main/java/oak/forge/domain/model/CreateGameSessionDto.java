package oak.forge.domain.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.*;

@Data
@AllArgsConstructor
public class CreateGameSessionDto {

    private int maxNumberOfPlayers;
    private String gameOwnerName;
    private String gameSessionName;
    private UUID playerId;

}
