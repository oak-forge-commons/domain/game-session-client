package oak.forge.domain.model;


import lombok.*;

import java.util.UUID;

@Getter
@RequiredArgsConstructor
public class Player {

    private final UUID playerId;
    private final String playerName;
    private final boolean playerGameOwner;

}
